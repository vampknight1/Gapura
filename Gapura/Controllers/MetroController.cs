﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Gapura.Controllers
{
    public class MetroController : Controller
    {
        //
        // GET: /Metro/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Metro/Details/5

        public ActionResult UI()
        {
            return View();
        }

        public ActionResult Form()
        {
            return View();
        }

        public ActionResult Chart()
        {
            return View();
        }

        public ActionResult Table()
        {
            return View();
        }
        public ActionResult Calendar()
        {
            return View();
        }

        public ActionResult Gallery()
        {
            return View();
        }

        public ActionResult Icon()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Messages()
        {
            return View();
        }

        public ActionResult Tasks()
        {
            return View();
        }

        public ActionResult Typography()
        {
            return View();
        }

        public ActionResult Widgets()
        {
            return View();
        }

        public ActionResult filemanager()
        {
            return View();
        }
    }

}