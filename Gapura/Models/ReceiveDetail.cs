//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gapura.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReceiveDetail
    {
        public int ReceiveDetailID { get; set; }
        public int ReceiveID { get; set; }
        public int ProductID { get; set; }
        public Nullable<int> StockBegin { get; set; }
        public int QtyReceive { get; set; }
        public Nullable<int> StockFinal { get; set; }
    }
}
